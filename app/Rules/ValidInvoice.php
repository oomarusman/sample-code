<?php

namespace App\Rules;

use App\Http\Repositories\InvoiceRepository;
use Illuminate\Contracts\Validation\Rule;
use Symfony\Component\HttpFoundation\ParameterBag;

/**
 * Class ValidInvoice
 *
 * @package App\Rules
 * @author  Omar Usman <oozman>
 */
class ValidInvoice implements Rule
{
    private $userId;
    private $invoiceRepository;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {

        $this->invoiceRepository = new InvoiceRepository;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string $attribute
     * @param  mixed  $value
     *
     * @return bool
     */
    public function passes($attribute, $value)
    {
        try {

            if ($this->userId) {

                $this->invoiceRepository->get(new ParameterBag(['id' => $value, 'user_id' => $this->userId]));
            } else {

                $this->invoiceRepository->get(new ParameterBag(['id' => $value]));
            }

            return true;
        } catch (\Exception $e) {

            return false;
        }
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        $error = 'The :attribute is not valid.';

        if ($this->userId) {

            $error = 'You don\'t have access to this invoice.';
        }

        return $error;
    }

    /**
     * Set record owner.
     *
     * @param $userId
     *
     * @return $this
     */
    public function ownedBy($userId)
    {

        $this->userId = $userId;

        return $this;
    }
}

#END OF PHP FILE
