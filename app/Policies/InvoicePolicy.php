<?php

namespace App\Policies;

use App\Invoice;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

/**
 * Class InvoicePolicy
 *
 * @package App\Policies
 * @author  Omar Usman <oozman>
 */
class InvoicePolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Create invoice.
     *
     * @param User $user
     *
     * @return bool
     */
    public function create(User $user)
    {

        return true;
    }

    /**
     * Show all records.
     *
     * @param User $user
     *
     * @return bool
     */
    public function showAll(User $user)
    {

        return true;
    }

    /**
     * Show invoice.
     *
     * @param User    $user
     * @param Invoice $invoice
     *
     * @return bool
     */
    public function show(User $user, Invoice $invoice)
    {

        return $user->id == $invoice->user_id;
    }

    /**
     * Update invoice.
     *
     * @param User    $user
     * @param Invoice $invoice
     *
     * @return bool
     */
    public function update(User $user, Invoice $invoice)
    {

        return $user->id == $invoice->user_id;
    }

    /**
     * Delete invoice.
     *
     * @param User    $user
     * @param Invoice $Invoice
     *
     * @return bool
     */
    public function delete(User $user, Invoice $invoice)
    {

        return $user->id == $invoice->user_id;
    }
}

#END OF PHP FILE