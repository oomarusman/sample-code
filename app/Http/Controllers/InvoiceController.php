<?php

namespace App\Http\Controllers;

use App\Http\Entities\Helper;
use App\Http\Repositories\InvoiceRepository;
use App\Http\Resources\ResponseResource;
use App\Invoice;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\ParameterBag;

/**
 * Class InvoiceController
 *
 * @package App\Http\Controllers
 * @author  Omar Usman <oozman>
 */
class InvoiceController extends Controller
{
    /**
     * Create invoice.
     *
     * @param Request           $request
     * @param InvoiceRepository $invoiceRepository
     * @param Helper            $helper
     *
     * @return $this|ResponseResource
     */
    public function store(Request $request, InvoiceRepository $invoiceRepository, Helper $helper)
    {

        try {

            $input = new ParameterBag($request->all());
            $input->set('user_id', Auth::user()->id);
            $input->set('created_at', $helper->dateNow()->getTimestamp());

            // Check if authorized.
            $this->authorize('create', Invoice::class);

            // Create invoice.
            $invoice = $invoiceRepository->create($input);

            $response = new ResponseResource($invoice);
            $response->setHttpCode(200);
            $response->setMessage('Invoice created.');

            return $response;
        } catch (\Exception $e) {

            $response = new ResponseResource($request);
            $response->setHttpCode(400);
            $response->setMessage($e->getMessage());

            return $response->response()->setStatusCode($response->getHttpCode());
        }
    }

    /**
     * Get list of invoices.
     *
     * @param Request           $request
     * @param InvoiceRepository $invoiceRepository
     *
     * @return $this|ResponseResource
     */
    public function showAll(Request $request, InvoiceRepository $invoiceRepository)
    {
        try {

            $input = new ParameterBag($request->all());
            $input->set('user_id', Auth::user()->id);

            // Check if authorized.
            $this->authorize('showAll', Invoice::class);

            // Get invoices.
            $invoices = $invoiceRepository->getAll($input);

            $response = new ResponseResource($invoices);
            $response->setHttpCode(200);
            $response->setMessage('List of Invoices.');

            return $response;
        } catch (\Exception $e) {

            $response = new ResponseResource($request);
            $response->setHttpCode(400);
            $response->setMessage($e->getMessage());

            return $response->response()->setStatusCode($response->getHttpCode());
        }
    }

    /**
     * Get invoice.
     *
     * @param                   $id
     * @param Request           $request
     * @param InvoiceRepository $invoiceRepository
     *
     * @return $this|ResponseResource
     */
    public function show($id, Request $request, InvoiceRepository $invoiceRepository)
    {

        try {

            // Get invoice.
            $invoice = $invoiceRepository->get(new ParameterBag(['id' => $id]));

            // Check if authorized.
            $this->authorize('show', $invoice);

            $response = new ResponseResource($invoice);
            $response->setHttpCode(200);
            $response->setMessage('Invoice found.');

            return $response;
        } catch (\Exception $e) {

            $response = new ResponseResource($request);
            $response->setHttpCode(400);
            $response->setMessage($e->getMessage());

            return $response->response()->setStatusCode($response->getHttpCode());
        }
    }

    /**
     * Update invoice.
     *
     * @param                   $id
     * @param Request           $request
     * @param InvoiceRepository $invoiceRepository
     * @param Helper            $helper
     *
     * @return $this|ResponseResource
     */
    public function update($id, Request $request, InvoiceRepository $invoiceRepository, Helper $helper)
    {

        try {

            $input = new ParameterBag($request->all());

            // Get invoice.
            $invoice = $invoiceRepository->get(new ParameterBag(['id' => $id]));

            // Check if authorized.
            $this->authorize('update', $invoice);

            // Remove parameters that shouldn't be updated.
            $input = new ParameterBag($helper->except($input->all(), ['user_id', 'created_at']));

            // Update invoice.
            $invoice = $invoiceRepository->update($invoice, $input);

            $response = new ResponseResource($invoice);
            $response->setHttpCode(200);
            $response->setMessage('Invoice updated.');

            return $response;
        } catch (\Exception $e) {

            $response = new ResponseResource($request);
            $response->setHttpCode(400);
            $response->setMessage($e->getMessage());

            return $response->response()->setStatusCode($response->getHttpCode());
        }
    }

    /**
     * Delete invoice.
     *
     * @param                   $id
     * @param Request           $request
     * @param InvoiceRepository $invoiceRepository
     *
     * @return $this|ResponseResource
     */
    public function destroy($id, Request $request, InvoiceRepository $invoiceRepository)
    {

        try {

            $input = new ParameterBag(['id' => $id]);

            // Get invoice.
            $invoice = $invoiceRepository->get($input);

            // Check if authorized.
            $this->authorize('delete', $invoice);

            // Delete invoice.
            $invoiceRepository->delete($input);

            $response = new ResponseResource($request);
            $response->setHttpCode(200);
            $response->setMessage('Invoice deleted.');

            return $response;
        } catch (\Exception $e) {

            $response = new ResponseResource($request);
            $response->setHttpCode(400);
            $response->setMessage($e->getMessage());

            return $response->response()->setStatusCode($response->getHttpCode());
        }
    }
}

#END OF PHP FILE