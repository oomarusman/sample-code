<?php namespace App\Http\Traits;

/**
 * Trait HasCustomResponse
 *
 * @package App\Http\Traits
 * @author Omar Usman <oozman>
 */
trait HasCustomResponse
{

    /**
     * Custom response.
     *
     * @param $content
     *
     * @return $this
     */
    public function generic($content)
    {
        return response([
            'http_code'     => $this->httpCode,
            'response_code' => $this->getResponseCode(),
            'message'       => $this->message,
            'data'          => ! empty($content) ? $content : null,
        ])->setStatusCode($this->httpCode);
    }
}

#END OF PHP FILE