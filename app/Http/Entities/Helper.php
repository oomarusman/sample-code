<?php namespace App\Http\Entities;

use Carbon\Carbon;

/**
 * Class Helper
 *
 * @package App\Http\Entities
 * @author  Omar Usman <oozman>
 */
class Helper
{

    /**
     * Get date now.
     *
     * @return Carbon
     */
    public function dateNow()
    {

        return Carbon::now(config('app.timezone'));
    }

    /**
     * Returns only array entries that are allowed.
     *
     * @param       $array
     * @param array $allowedKeys
     *
     * @return array
     */
    public function only($array, $allowedKeys = [])
    {

        return array_intersect_key($array, array_flip($allowedKeys));
    }

    /**
     * Returns array except for a disallowed key.
     *
     * @param       $array
     * @param array $disallowKeys
     *
     * @return array
     */
    public function except($array, $disallowKeys = [])
    {

        return array_diff_key($array, array_flip($disallowKeys));
    }

    /**
     * Remove empty values.
     *
     * @param $array
     *
     * @return array
     */
    public function removeEmpty($array)
    {

        return array_filter($array, function ($value) {

            // Don't remove if number or boolean value.
            if (is_numeric($value) || is_bool($value)) {

                return true;
            }

            return ! empty($value);
        });
    }

    /**
     * Get complete raw sql with bindings.
     *
     * @param $sql
     * @param $bindings
     *
     * @return null|string|string[]
     */
    public function toSql($sql, $bindings)
    {

        foreach ($bindings as $binding) {

            $value = is_numeric($binding) ? $binding : "'".$binding."'";
            $sql   = preg_replace('/\?/', $value, $sql, 1);
        }

        return $sql;
    }

    /**
     * Mask a string.
     *
     * @param      $str
     * @param int  $start
     * @param null $length
     *
     * @return mixed
     */
    public function mask($str, $start = 0, $length = null)
    {

        $mask = preg_replace("/\S/", "*", $str);

        if (is_null($length)) {

            $mask = substr($mask, $start);
            $str  = substr_replace($str, $mask, $start);
        } else {

            $mask = substr($mask, $start, $length);
            $str  = substr_replace($str, $mask, $start, $length);
        }

        return $str;
    }
}

#END OF PHP FILE