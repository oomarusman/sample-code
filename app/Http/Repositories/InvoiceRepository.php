<?php namespace App\Http\Repositories;

use App\Http\Entities\Helper;
use App\Http\Entities\Token;
use App\Invoice;
use Symfony\Component\HttpFoundation\ParameterBag;
use Symfony\Component\Routing\Exception\InvalidParameterException;
use Symfony\Component\Translation\Exception\NotFoundResourceException;

/**
 * Class InvoiceRepository
 *
 * @package App\Http\Repositories
 * @author  Omar Usman <oozman>
 */
class InvoiceRepository
{
    private $token;
    private $helper;

    public function __construct()
    {

        $this->token  = new Token;
        $this->helper = new Helper;
    }

    /**
     * Create invoice.
     *
     * @param ParameterBag $input
     *
     * @return Invoice
     */
    public function create(ParameterBag $input)
    {

        $invoice = new Invoice;

        if ( ! $invoice->valid($input->all())) {

            throw new InvalidParameterException('Unable to create invoice. '.$invoice->errors()->first());
        }

        $invoice->fill($input->all());
        $invoice->save();

        return $invoice;
    }

    /**
     * Get records by where clause.
     *
     * @param ParameterBag $where
     *
     * @return mixed
     */
    public function getAll(ParameterBag $where)
    {

        $invoice = new Invoice;

        $limit = $where->get('limit', 100);

        $where = new ParameterBag($this->helper->except($where->all(), ['limit', 'page']));

        foreach ($where as $field => $value) {

            $invoice = $invoice->where($field, $value);
        }

        return $invoice->paginate($limit);
    }

    /**
     * Get invoice.
     *
     * @param ParameterBag $where
     *
     * @return Invoice
     */
    public function get(ParameterBag $where)
    {

        if ($where->count() <= 0) {

            throw new InvalidParameterException('Invoice not found.');
        }

        $invoice = new Invoice;

        foreach ($where as $field => $value) {

            $invoice = $invoice->where($field, $value);
        }

        $invoice = $invoice->first();

        if ( ! $invoice) {

            throw new NotFoundResourceException('Invoice not found.');
        }

        return $invoice;
    }

    /**
     * Update invoice.
     *
     * @param Invoice      $invoice
     * @param ParameterBag $input
     *
     * @return Invoice
     */
    public function update(Invoice $invoice, ParameterBag $input)
    {

        // Remove empty input.
        $input = new ParameterBag($this->helper->removeEmpty($input->all()));

        $invoice->noRequiredValidationRules();

        if ( ! $invoice->valid($input->all())) {

            throw new InvalidParameterException('Unable to update invoice. '.$invoice->errors()->first());
        }

        $invoice->fill($input->all());
        $invoice->save();

        return $invoice;
    }

    /**
     * Delete invoice.
     *
     * @param ParameterBag $where
     *
     * @return bool|null
     * @throws \Exception
     */
    public function delete(ParameterBag $where)
    {

        $invoice = $this->get($where);

        $isDeleted = $invoice->delete();

        if ( ! $isDeleted) {

            throw new \Exception('Unable to delete invoice.');
        }

        return $isDeleted;
    }
}

#END OF PHP FILE