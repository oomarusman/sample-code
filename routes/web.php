<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::post('invoices', ['uses' => 'InvoiceController@store']);
Route::get('invoices', ['uses' => 'InvoiceController@showAll']);
Route::get('invoices/{id}', ['uses' => 'InvoiceController@show']);
Route::put('invoices/{id}', ['uses' => 'InvoiceController@update']);
Route::delete('invoices/{id}', ['uses' => 'InvoiceController@destroy']);